import { Table, TableContainer, TableHead, TableRow, TableCell, TableBody, Container, Grid, Paper, Pagination } from "@mui/material";
import { Button } from '@mui/material'
import { useEffect, useState } from "react";
import ModalDetail from "./modal-detail";

function DataTable() {
    const getData = async () => {
        const response = await fetch("https://jsonplaceholder.typicode.com/posts");

        const data = await response.json();

        return data;
    }
    
    const limitRow = 10; //10 dòng cho mỗi trang
    const [noPage, setNoPage] = useState(1)
    const [posts, setPosts] = useState([])
    const [page, setPage] = useState(1);
    const changePage = (event, value) => {
        setPage(value);
    }
    useEffect(() => {
        getData().then((data) => {
            setNoPage(Math.ceil(data.length/limitRow));
            setPosts(data.slice(page*limitRow - limitRow,page*limitRow));
        }).catch((error) => {
            console.log(error);
        });
    }, [page]);
    const [showModal, setShowModal] = useState(false);
    const [data, setData] = useState({});
    const handleClickBtn = (data) => {
        console.log(data);
        setShowModal(true);
        setData(data);
    }
    return (
        <Container>
            <Grid container marginTop={10}>
                <Grid item xs={12} md={12} lg={12} sm={12}>
                    <TableContainer component={Paper}>
                        <Table sx={{ minWidth: 650 }} aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell align="center" sx={{fontWeight:'bold'}}>STT</TableCell>
                                    <TableCell align="center" sx={{fontWeight:'bold'}}>ID</TableCell>
                                    <TableCell align="center" sx={{fontWeight:'bold'}}>Title</TableCell>
                                    <TableCell align="center" sx={{fontWeight:'bold'}}>Body</TableCell>
                                    <TableCell align="center" sx={{fontWeight:'bold'}}>Detail</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {posts.map((row, index) => (
                                    <TableRow key={index}>
                                        <TableCell component="th" scope="row">
                                            {index + 1}
                                        </TableCell>
                                        <TableCell align="left">{row.id}</TableCell>
                                        <TableCell align="left">{row.title}</TableCell>
                                        <TableCell align="left">{row.body}</TableCell>
                                        <TableCell align="center"><Button variant="contained" color="success" onClick={() => handleClickBtn(row)}>Detail</Button></TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Grid>
                <Grid item xs={12} md={12} lg={12} sm={12} marginY={2}>
                    <Pagination count={noPage} color="primary" defaultPage={page} onChange={changePage}/>
                </Grid>
                <ModalDetail show={showModal} setShow = {setShowModal} data={data}/>
            </Grid>
        </Container>
    )
}
export default DataTable;