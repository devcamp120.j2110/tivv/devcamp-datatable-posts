import { Modal, Box, TextField } from '@mui/material'
const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};
function ModalDetail({show, setShow, data}) {
    const handleClose = () => {
        setShow(false);
    }
    return (
        <div>
            <Modal
                open={show}
                onClose={handleClose}
                aria-labelledby="modal-title"
                aria-describedby="modal-description"
            >
                <Box sx={style}>
                    <TextField sx={{pb:2}} id="outlined-basic" label="Id" variant="outlined" fullWidth value={data.id}/>
                    <TextField sx={{pb:2}} id="outlined-basic" label="Title" variant="outlined" fullWidth value={data.title}/>
                    <TextField sx={{pb:2}} id="outlined-basic" label="Body" variant="outlined" fullWidth value={data.body}/>
                </Box>
            </Modal>
        </div>
    )
}
export default ModalDetail;